package sto.luhn;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 Models the types of credit card accepted by the application, including the
 logic of deciding type of card based on card number.
 */
public enum CreditCardType {
  Amex("AMEX", CreditCardType::isAmex),
  Discover("Discover", CreditCardType::isDiscover),
  Mastercard("MasterCard", CreditCardType::isMastercard),
  Visa("VISA", CreditCardType::isVisa),
  ;

  private String description;
  private Function<String, Boolean> cardTypePredicate;

  CreditCardType(
    String description,
    Function<String, Boolean> cardTypePredicate
  ) {
    this.description = description;
    this.cardTypePredicate = cardTypePredicate;
  }

  public String getDescription() {
    return description;
  }

  public Function<String, Boolean> getCardTypePredicate() {
    return cardTypePredicate;
  }

  /**
   Returns the card type of the given credit card number.
   @param number may be null or empty
   @return none if type cannot be decided
   */
  public static Optional<CreditCardType> cardTypeFor(String number){
    return Stream.of(CreditCardType.values()).
      filter(i->i.cardTypePredicate.apply(number)).findFirst();
  }

  /**
   Returns the description of the card type for the given credit card number.
   @param number may be null or empty
   @return none if type cannot be decided
   */
  public static Optional<String> descriptionFor(String number){
    return cardTypeFor(number).map(CreditCardType::getDescription);
  }

  /** @param number may be null or empty */
  static boolean isAmex(String number){
    if( number == null ){
      return false;
    }

    if( number.startsWith("34") || number.startsWith("37") ){
      return number.length() == 15;
    }

    return false;
  }

  /** @param number may be null or empty */
  static boolean isDiscover(String number){
    if( number == null ){
      return false;
    }

    if( number.startsWith("6011") ){
      return number.length() == 16;
    }

    return false;
  }

  /** @param number may be null or empty */
  static boolean isMastercard(String number){
    if( number == null ){
      return false;
    }

    // this is nasty, come back for this later maybe
    if( number.startsWith("51") ||
      number.startsWith("52") ||
      number.startsWith("53") ||
      number.startsWith("54") ||
      number.startsWith("55")
    ){
      return number.length() == 16;
    }

    return false;
  }

  /** @param number may be null or empty */
  static boolean isVisa(String number){
    if( number == null ){
      return false;
    }

    if( number.startsWith("4") ){
      return number.length() == 13 || number.length() == 16;
    }

    return false;
  }

}
