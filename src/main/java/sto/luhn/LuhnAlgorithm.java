package sto.luhn;


import java.util.ArrayList;
import java.util.List;
import sto.util.Check;

import static sto.luhn.LuhnAlgorithm.Util.convertToNumber;
import static sto.luhn.LuhnAlgorithm.Util.reverse;

/**
 Implements the
 <a href="https://en.wikipedia.org/wiki/Luhn_algorithm">Luhn algorithm</a> for
 verifying credit card numbers.
 <p/>
 Generates a lot of heap noise for what it does - if performance is an issue,
 consider re-writing in zero-allocation style.  After measuring.
 */
public class LuhnAlgorithm {

  /**
   @param digits a string of digits suitable for validation, input is
   expected to be already sanitised and suitable for validation.
   @throws RuntimeException if passed unexpected input
   */
  public static boolean isValid(char[] digits){
    Check.notNull("digits may not be null", digits);
    Check.isTrue("digits is too short", digits.length > 12);

    char[] reversedDigits = reverse(digits);
    List<Integer> totalInputs = new ArrayList<>();

    for( int i=0; i <= reversedDigits.length-1; i +=2 ){

      totalInputs.add(convertToNumber(reversedDigits[i]));

      if( reversedDigits.length >= i+2 ){
        int doubleSum = convertToNumber(reversedDigits[i + 1]) * 2;
        // if multi-digit, add the digits together
        if( doubleSum > 9 ){
          doubleSum = doubleSum - 9;
        }
        totalInputs.add(doubleSum);
      }

    }

    int sum = totalInputs.stream().mapToInt(i -> i).sum();

    return sum % 10 == 0;
  }


  /** Public for unit testing, not intended for general usage. */
  static public class Util {

    /**
     Given a "digit" character as per {@link Character#isDigit(char)}
     returns the integer "number" for that character (i.e. "0" == 0, "9" == 0,
     etc.)

     @throws RuntimeException if input is not valid
     */
    public static Integer convertToNumber(char c){
      Check.isTrue("method only accapts digit chars", Character.isDigit(c));
      return Character.getNumericValue(c);
    }

    /**
     Reverses the given array.

     @return may return null, may return same or different array.
     */
    public static char[] reverse(char[] chars) {
      if( chars == null || chars.length < 2 ){
        return chars;
      }
      return new StringBuilder(new String(chars)).
        reverse().toString().toCharArray();
    }
  }

}
