package sto.luhn;

import java.util.Optional;
import java.util.regex.Pattern;

import static sto.luhn.CreditCardNumberValidator.Util.formatResult;
import static sto.luhn.CreditCardNumberValidator.Util.sanitiseCardNumber;
import static sto.util.StringUtil.isBlank;


public class CreditCardNumberValidator {

  /** Validates a credit card as per the challenge requirements. */
  public static String validate(String input){
    if(input == null ){
      return formatResult("Invalid", "<null>", false);
    }

    String sanitisedDigits = sanitiseCardNumber(input).orElse("");
    if( isBlank(sanitisedDigits) ){
      return formatResult("Invalid", input, false);
    }

    String description =
      CreditCardType.descriptionFor(sanitisedDigits).orElse("Unknown");
    boolean isValid = LuhnAlgorithm.isValid(sanitisedDigits.toCharArray());

    return formatResult(description, sanitisedDigits, isValid);
  }


  /** Public for unit testing, not intended for general usage. */
  public static class Util {

    private static final Pattern NONDIGIT_PATTERN = Pattern.compile("\\D+");
    private static final Pattern WHITSPACE_PATTERN =
      Pattern.compile("\\p{Space}+");
    private static final Pattern PUNCTUATION_PATTERN =
      Pattern.compile("\\p{Punct}+");

    /**
     Sanitise the input to a string of digit characters.
     @return none if input appears to be obviously invalid
     */
    public static Optional<String> sanitiseCardNumber(String input){
      // disallow blank numbers
      if( isBlank(input) ){
        return Optional.empty();
      }

      // allow white space separated numbers
      input = WHITSPACE_PATTERN.matcher(input).replaceAll("");
      // allow punctuation separated numbers
      input = PUNCTUATION_PATTERN.matcher(input).replaceAll("");

      // disallow numbers made up entirely of space/punctuation
      if( isBlank(input) ){
        return Optional.empty();
      }

      // disallow any other input with non-digit chars
      if( NONDIGIT_PATTERN.matcher(input).find() ){
        return Optional.empty();
      }

      return Optional.of(input);
    }

    /**
     Format the given input for the expected validation output.
     */
    public static String formatResult(
      String type, String number, boolean valid
    ){
      String cardData = String.format("%s: %s", type, number);
      // 28 to match (roughly) example output shown in challenge
      return String.format("%-28s (%s)", cardData, valid?"valid":"invalid");
    }
  }

}
