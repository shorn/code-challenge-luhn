package sto.util;

/**
 Static utility methods for working with strings.
 */
public class StringUtil {

  /** Null is blank. Empty string is blank.  All whitespace is blank.
   */
  public static boolean isBlank(String target) {
    if( isNullOrEmpty(target) ){
      return true;
    }
    return isWhitespace(target);
  }

  /** Null is not a value.  Empty string is not a value.  Whitespace is not a
   value.
   */
  public static boolean hasValue(String target) {
    return !isBlank(target);
  }

  /** from apache commns */
  private static boolean isNullOrEmpty(String string) {
    return string == null || string.length() == 0;
  }

  /** from guava */
  private static boolean isWhitespace(final CharSequence cs) {
    if (cs == null) {
      return false;
    }
    final int sz = cs.length();
    for (int i = 0; i < sz; i++) {
      if (Character.isWhitespace(cs.charAt(i)) == false) {
        return false;
      }
    }
    return true;
  }

}
