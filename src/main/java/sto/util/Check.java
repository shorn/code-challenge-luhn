package sto.util;

/**
 Check methods throw a RuntimeException if they fail.
 */
public class Check {
  public static void isTrue(String msg, boolean value){
    if( !value ){
      throw new RuntimeException(msg);
    }
  }

  public static void notNull(String msg, Object value){
    if( value == null ){
      throw new RuntimeException(msg);
    }
  }
}
