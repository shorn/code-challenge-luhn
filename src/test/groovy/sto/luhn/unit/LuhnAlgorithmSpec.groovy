package sto.luhn.unit

import spock.lang.Specification
import sto.luhn.LuhnAlgorithm

import static sto.luhn.LuhnAlgorithm.Util.convertToNumber
import static sto.luhn.LuhnAlgorithm.Util.reverse

class LuhnAlgorithmSpec extends Specification{
  def utilReverse(){
    expect:
    reverse(null) == null
    reverse(chars("")) == chars("")
    reverse(chars("a")) == chars("a")
    reverse(chars("ab")) == chars("ba")
    reverse(chars("ab")) == chars("ba")
    reverse(chars("123")) == chars("321")
  }

  def utilConvertToNumber(){
    expect:
    convertToNumber('0' as char) == 0
    convertToNumber('1' as char) == 1
    convertToNumber('9' as char) == 9
  }

  def validateSanitisedValidCardNumbers(){
    when: "challenge input valid cards"
    then:
    LuhnAlgorithm.isValid(chars("4408041234567893"))
    LuhnAlgorithm.isValid(chars("4012888888881881"))
    LuhnAlgorithm.isValid(chars("378282246310005"))
    LuhnAlgorithm.isValid(chars("6011111111111117"))
    LuhnAlgorithm.isValid(chars("5105105105105100"))

    when: "stripe test cards"
    then:
    LuhnAlgorithm.isValid(chars("4242424242424242")) // visa
    LuhnAlgorithm.isValid(chars("4000056655665556")) // visa debit
    LuhnAlgorithm.isValid(chars("5555555555554444")) // MC
    LuhnAlgorithm.isValid(chars("30569309025904"))   // diners
    LuhnAlgorithm.isValid(chars("3530111333300000")) // JCB
  }

  def validateSanitisedInvalidCardNumbers(){
    when: "challenge input invalid cards"
    then:
    !LuhnAlgorithm.isValid(chars("4111111111111"))
    !LuhnAlgorithm.isValid(chars("5105105105105106"))
    !LuhnAlgorithm.isValid(chars("9111111111111111"))
  }

  private static char[] chars(String s) {
    s.toCharArray()
  }
}
