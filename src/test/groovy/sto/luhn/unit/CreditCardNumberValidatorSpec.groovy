package sto.luhn.unit

import spock.lang.Specification

import static sto.luhn.CreditCardNumberValidator.Util.formatResult
import static sto.luhn.CreditCardNumberValidator.Util.sanitiseCardNumber
import static sto.luhn.CreditCardNumberValidator.validate

class CreditCardNumberValidatorSpec extends Specification{
  def utilSanitiseInput(){
    expect:
    !sanitiseCardNumber("").isPresent()
    !sanitiseCardNumber("a").isPresent()
    sanitiseCardNumber("1").get() == "1"
    sanitiseCardNumber("1 ").get() == "1"
    sanitiseCardNumber(" 1").get() == "1"
    sanitiseCardNumber(" 1 ").get() == "1"
    !sanitiseCardNumber(" 1a a").isPresent()
    sanitiseCardNumber("5105 1051 0510 5106").get() == "5105105105105106"
    sanitiseCardNumber("5105-1051-0510-5106").get() == "5105105105105106"
    sanitiseCardNumber("5105_1051 0510.5106").get() == "5105105105105106"
  }

  def utilFormat(){
    expect:
    formatResult("x", "y", true) == "x: y                         (valid)"
  }

  def validate(){
    when: "bad data"
    then:
    validate(null) == "Invalid: <null>              (invalid)"
    validate("")   == "Invalid:                     (invalid)"
    validate("a")  == "Invalid: a                   (invalid)"

    when: "valid number"
    then:
    validate("4111111111111111") == "VISA: 4111111111111111       (valid)"

    when: "invalid number"
    then:
    validate("5105 1051 0510 5106") == "MasterCard: 5105105105105106 (invalid)"
    validate("5105a1051a0510a5106") == "Invalid: 5105a1051a0510a5106 (invalid)"
    validate("a4111111111111111")   == "Invalid: a4111111111111111   (invalid)"
    validate("4111111111111111a")   == "Invalid: 4111111111111111a   (invalid)"
    validate("4242k4242k4242k4242") == "Invalid: 4242k4242k4242k4242 (invalid)"
  }
}
