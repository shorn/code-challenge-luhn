[TOC]

# Introduction

Contains code implement the Luhn algorithm for credit card number validation.
 
# Project structure

## Build 
The [build](/build.gradle) is a [Gradle](https://gradle.org) project.  
It includes the gradle-wrapper so it should run on any Windows / Linux / 
MacOs box that has a JDK 8 available.

## Code
The implementation code is Java 8, no libraries. 
 
The test code is Groovy, using the [Spock framework](http://spockframework.org/).
 

## Implementation
There are three main implementation classes:

- [CreditCardNumberValidator](/src/main/java/sto/luhn/CreditCardNumberValidator.java)
    -  This acts as the "main" entry-point, it runs the CC validation logic
    on a single string - uses CreditCardType and LuhnAlgorithm to do it's job. 
- [CreditCardType](/src/main/java/sto/luhn/CreditCardType.java)
    -  Models the types of credit cards accepted (Visa, etc.) and the logic 
    for deciding which kind of card is being dealt with.
-  [LuhnAlgorithm](/src/main/java/sto/luhn/LuhnAlgorithm.java)
    -  Implements the Luhn algorithm on a given arrays of digits.

## Tests

There are two types of tests:

- unit tests - [sto.luhn.unit.*](/src/test/groovy/sto/luhn/unit)
- functional test - [ChallengeSpec](/src/test/groovy/sto/luhn/functional/ChallengeSpec.groovy)
    -  implements the coding challenge

Both sets of tests are run by the Gradle `test` task.    
     

# Setup 

## Pre-requisites
- git
- JDK 8 (I built with Java 1.8.0_111)

## Building and running from the command line
- clone to directory
    - put the source in something like c:/projects/code-challenge-luhn
- run `./gradlew test` from within the root directory of the repo 
    - as long as JDK 8 is available on the path (or JAVA_HOME is set) 
    this will run both the unit tests and the functional test as per 
    the code challenge 


## IDEA setup
- open [/build.gradle](/build.gradle) as a project
- run the test task from the Gradle tool window 


# Download 
- Code can be downloaded from: 
https://bitbucket.org/shorn/code-challenge-luhn/downloads/




      